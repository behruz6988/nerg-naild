/**
 * jQuery.sliderota
 * Version 1.0
 * Copyright (c) 2010 c.bavota - http://bavotasan.com
 * Dual licensed under MIT and GPL.
 * Date: 04/22/2010
**/
(function($) {
	$.fn.sliderota = function(options) {
		// setting the defaults
		// $("#sliderota").simpleSlider({speed: 3000, timer: 6000, slideshow: true, easing: "easeInOutQuint"});
		var defaults = {
			speed: 3000,
			timer: 6000,
			slideshow: true,
			easing: 'easeInOutQuint'
		};	
		var options = $.extend(defaults, options);
		
		// and the plugin begins
		return this.each(function() {
			// initializing the variables
			var loop, counter, obj, totalWidth, itemWidth, curLeft, itemNum, limit;

			// creating the object variable
			obj = $(this);
			
			counter = 0;
			itemWidth = obj.find(".slides").width();
			itemNum = obj.find(".slides a").length;
			totalWidth = (itemNum + 1) * itemWidth;
			limit = -(itemNum * itemWidth);

			obj
				.find(".slides").css({
					width: totalWidth+"px"
				})
				.end()
				.append("<ul></ul>")
				.find(".slides a").each(function() {
					$("<li><a href='javascript:void(0)' class='slide_"+counter+"'></a></li>").appendTo(obj.find("ul"));
					counter++;
				})
				.end()
				.find("ul li:eq(0)").addClass("selected")
				.end()
				.find(".slides a:eq(0)").clone().appendTo(".slides", obj)
				.end()
				// creating the click event
				.find("ul li a").live('click', function() {
					obj.find("ul li").each(function() {
						$(this).removeClass("selected");
					});
					curLeft = $(this).attr('class').replace("slide_","");
					curLeft = -(curLeft * itemWidth);
					
					$(this).parent().addClass("selected");		
					obj // selecting the object
						.find(".slides").stop().animate({ left: curLeft+"px" }, options.speed, options.easing)
						.end()
						.find(".controls a.play").show()
						.end()
						.find(".controls a.pause").hide();
					clearTimeout(loop);
				});
			
			// slideshow functionality
			if(options.slideshow) {
				// adding the play/pause controls
				obj // selecting the object
					.append("<div class='controls'></div>")
					.find(".controls")
						.append("<a href='javascript:void(0)' class='play'></a>")
						.append("<a href='javascript:void(0)' class='pause'></a>")					
					.end()
					// creating the play click event
					.find(".controls .play").click(function() {
						loop = setTimeout(function() { moveSlides(); }, options.timer);
						$(this).hide();
						obj.find(".controls .pause").show();
					})
					.end()
					// creating the pause click event
					.find(".controls .pause").click(function() {
						clearTimeout(loop);
						obj.find(".controls .play").show();
						$(this).hide();
					});	
				
				// creating the loop for the slideshow
				loop = setTimeout(function() { moveSlides(); }, options.timer);
	
				// creating the function to move the slides
				function moveSlides() {
					obj.find("ul li").each(function() {
						$(this).removeClass("selected");
					})
					curLeft = obj.find(".slides").css('left').replace("px", "");
					curItem = (Math.abs(curLeft) / itemWidth)+1;
					if(curLeft==limit+itemWidth) {
						obj.find('ul li:eq(0)').addClass("selected");
					} else {
						obj.find('ul li a.slide_'+curItem).parent().addClass("selected");
					}
					curLeft = curLeft - itemWidth;
					obj.find(".slides").stop().animate({ left: curLeft+"px" }, options.speed, options.easing, function() {
						if(curLeft==limit) { obj.find(".slides").css({ left: "0px" }); }
					});
					loop = setTimeout(function() { moveSlides(); }, options.timer);
				};
			};
		});
	};
})(jQuery);